
;set meta for mac
(setq mac-option-key-is-meta nil)
(setq mac-command-key-is-meta t)
(setq mac-command-modifier 'meta)
(setq mac-option-modifier nil)

;; path to where plugins are kept
(add-to-list 'load-path "~/.emacs.d/settings")
(add-to-list 'load-path "/Users/thorey/.emacs.d/emacs-for-python/") 
(setq plugin-path "~/.emacs.d/el-get/")


;; define various custom functions
(require 'custom-functions)

;; install dependencies with el-get
(require 'el-get-settings)


;; configure general settings
(require 'general-settings)

;---------------;
;;; Utilities ;;;
;---------------;

;; Python
;(load-file "/Users/thorey/.emacs.d/emacs-for-python/epy-init.el")

(require 'epy-setup)      ;; It will setup other loads, it is required!
(require 'epy-python)     ;; If you want the python facilities [optional]
(require 'epy-completion) ;; If you want the autocompletion settings [optional]
(require 'epy-editing)    ;; For configurations related to editing [optional]
(require 'epy-bindings)   ;; For my suggested keybindings [optional]
(require 'epy-nose)       ;; For nose integration

;; Websocket
(include-plugin "websocket")
(require 'websocket)

;-----------;
;;; Modes ;;;
;-----------;

;; Ido mode
(require 'ido)
(ido-mode 1)

;; MuMaMo
(require 'mumamo-settings)

;; Markdown mode
(require 'markdown-settings)

;; LaTeX and Auctex
(require 'latex-settings)

;; ---------------------------
;; -- Custom functions --
;; ---------------------------

;; Reload emacs

(defun reload ()
  (interactive)
  (load-file "~/.emacs"))


;---------------------------------------------------------------------
;; Put auto 'custom' changes in a separate file (this is stuff like
;; custom-set-faces and custom-set-variables)
(load 
 (setq custom-file (expand-file-name "settings/custom.el" user-emacs-directory))
 'noerror)

(put 'downcase-region 'disabled nil)
(put 'upcase-region 'disabled nil)
